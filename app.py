from tools.setup import Flask, Api, secret_key

# API ROUTES
from routes.route_server import ServerConsult, server

app = Flask(__name__)
api = Api(app)
app.config["SECRET_KEY"] = secret_key

api.add_resource(ServerConsult, "/server/<string:option>")

if __name__ == "__main__":
    app.run(debug = True, port = 5000)
    server.closeConnection()