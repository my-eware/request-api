import requests, time

class APIFarmer:
	@classmethod
	def get_disk_status(cls):
		requests.get(url = "http://127.0.0.1:5000/server/disk_status")
	@classmethod
	def get_memory_status(cls):
		requests.get(url = "http://127.0.0.1:5000/server/memory_status")
	@classmethod
	def get_top_processes_status(cls):
		requests.get(url = "http://127.0.0.1:5000/server/top_processes_status")

if __name__ == "__main__":
	start = time.time()
	end = start + 60*60
	while True:
		APIFarmer.get_disk_status()
		APIFarmer.get_memory_status()
		APIFarmer.get_top_processes_status()
		time.sleep(15)
		start = time.time()