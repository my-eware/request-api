###################################################
#
# config.py - Configuracion general
#
###################################################

import os, time, json, socket, sqlite3, requests, logging, jwt
from paramiko import SSHClient, AutoAddPolicy, ssh_exception
from flask import Flask, redirect, request, jsonify
from flask_restful import Resource, reqparse, Api
from functools import wraps

###################################################
# Configuracion de la App
###################################################
APP_NAME = "REQUEST API"

remote_server = json.load(open("../config.json"))["server"]
secret_key = json.load(open("../config.json"))["ACCESS_TOKEN_SECRET"]