from tools.setup import wraps, jwt, secret_key, request

def token_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        token = None
        if "authorization" in request.headers:
            token = request.headers["authorization"].split(" ")[1]

        if not token:
            return { "message" : "Token is missing"}, 401

        try:
            jwt.decode(token, secret_key)
        except:
            return { "message": "Token is not valid"}, 401

        return f(*args, **kwargs)
    
    return wrapper