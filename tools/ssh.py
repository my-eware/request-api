from tools.setup import SSHClient, AutoAddPolicy

class RemoteServer:
    def __init__(self, username, passwd, hostname, port = None):
        self.__passwd = str(passwd)
        self.__username = str(username)
        self.__port = port
        self.__hostname = str(hostname)

        self.client = SSHClient()
        self.client.set_missing_host_key_policy(AutoAddPolicy())

        try:
            if (self.port != None or self.port!=""):
                self.client.connect(self.hostname, port=self.port, username=self.username, password=self.passwd, look_for_keys=False)
            else:
                self.client.connect(self.hostname, username=self.username, password=self.passwd)
        except Exception as e:
            print(e)

    @property
    def passwd(self):
        return self.__passwd

    @passwd.setter
    def passwd(self, passwd):
        if passwd  == None or passwd == "":
            raise ValueError("Can't leave password empty")
        else:
            self.__passwd = passwd

    @property
    def username(self):
        return self.__username

    @username.setter
    def username(self, username):
        if username == None or username == "":
            raise ValueError("Can't leave username empty")
        else:
            self.__username = username

    @property
    def hostname(self):
        return self.__hostname

    @hostname.setter
    def hostname(self, hostname):
        if hostname == None or hostname == "":
            raise ValueError("Can't leave hostname empty")
        else:
            self.__hostname = hostname

    @property
    def port(self):
        return self.__port

    @port.setter
    def port(self, port):
        if type(port) != int and port != None:
            raise ValueError("Port is not a number")
        else:
            self.__port = port

    def getDiskStatus(self):
        try:
            stdin, stdout, stderr = self.client.exec_command("df -H -v /")
            return stdout

        except AttributeError:
            return None
        
    def getMemStatus(self):
        try:
            stdin, stdout, stderr = self.client.exec_command("top -b -n 1 -o %MEM | head -n 4 | tail -n 1")
            return stdout

        except AttributeError:
            return None

    def getTopThreeAppsStatus(self):
        try:
            stdin, stdout, stderr = self.client.exec_command("top -b -n 1 -o %MEM | head -n 10 | tail -n 4")
            return stdout
            
        except AttributeError:
            return None

    def closeConnection(self):
        self.client.close()