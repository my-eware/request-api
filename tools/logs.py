import time, os

class Logs:
    @classmethod
    def disk(cls, status_dict):
        if not os.path.exists("logs/disk_log.txt"):
                init_file = open("logs/disk_log.txt","x")
                init_file.write("TIMESTAMP|registry_type|size|used|available|used_p|location\n")
                init_file.close()
        file = open("logs/disk_log.txt", "a")
        file.write(str(int(time.time())) + "|")
        file.write("disk" + "|")
        file.write("|".join([status_dict[disk][key] for disk in status_dict.keys() for key in status_dict[disk]]) + 
                    "\n")
        file.close()
    
    @classmethod
    def memory(cls, status_dict):
        if not os.path.exists("logs/memory_log.txt"):
                init_file = open("logs/memory_log.txt","x")
                init_file.write("TIMESTAMP|registry_type|total|free|used|buff/cache\n")
                init_file.close()
        file = open("logs/memory_log.txt", "a")
        file.write(str(int(time.time())) + "|")
        file.write("memory" + "|")
        file.write("|".join([str(status_dict[list(status_dict.keys())[0]][key]) for key in status_dict[list(status_dict.keys())[0]]]) + 
                    "\n")
        file.close()

    @classmethod
    def top_processes(cls, status_dict):
        if not os.path.exists("logs/top_processes_log.txt"):
                init_file = open("logs/top_processes_log.txt","x")
                init_file.write("TIMESTAMP|registry_type|pid|user|pr|ni|virtual_mem|res|shr|status|cpu_use_p|mem_use_p|time|command\n")
                init_file.close()
        file = open("logs/top_processes_log.txt", "a")

        for pid in status_dict.keys():
            temporal = [pid]
            for stat in status_dict[pid]:
                temporal.append(status_dict[pid][stat])
            file.write(str(int(time.time())) + "|")
            file.write("Top_processes" + "|")
            file.write("|".join(temporal) + "\n")
        
        file.close()